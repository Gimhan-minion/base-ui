/*
 * Copyright (C) 2020. Entgra (Pvt) Ltd, https://entgra.io
 * All Rights Reserved.
 *
 * Unauthorized copying/redistribution of this file, via any medium
 * is strictly prohibited.
 * Proprietary and confidential.
 *
 * Licensed under the Entgra Commercial License,
 * Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 * You may obtain a copy of the License at
 * https://entgra.io/licenses/entgra-commercial/1.0
 */

import React from 'react';

import { UserOutlined } from '@ant-design/icons';

import { Layout, Menu, Avatar, notification } from 'antd';
import { Switch, Link } from 'react-router-dom';
import RouteWithSubRoutes from '../../components/RouteWithSubRoutes';
import styles from './styles.module.css';
import { withConfigContext } from '../../components/ConfigContext';
import axios from 'axios';

const { Header, Content, Sider } = Layout;

export const RoutesContext = React.createContext();

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      routes: props.routes,
      selectedKeys: [],
      currentRoute: 'sample',
    };
    this.logo = this.props.context.theme.logo;
    this.config = this.props.context;
  }

  setCurrentRoute = (key) => {
    this.setState({
      currentRoute: key,
    });
  };

  logout = () => {
    axios
      .post(window.location.origin + this.props.context.serverConfig.logoutUri)
      .then((res) => {
        // if the api call status is correct then user will logout and then it goes to login page
        if (res.status === 200) {
          window.location =
            window.location.origin +
            `/${this.config.appName}/login?redirect=${encodeURI(
              window.location.href,
            )}`;
        }
      })
      .catch(function (error) {
        notification.error({
          message: 'There was a problem',
          duration: 0,
          description: 'Error occurred while trying to logout.',
        });
      });
  };

  render() {
    return (
      <Layout>
        <Sider
          style={{
            overflow: 'auto',
            height: '100vh',
            position: 'fixed',
            zIndex: 20,
          }}
          breakpoint="lg"
          collapsedWidth="0"
        >
          <div>
            <Link to={`/${this.config.appName}`}>
              <div className={styles.logo}>
                <img src={this.logo} alt="Logo" />
              </div>
            </Link>
          </div>
          <Menu
            theme="dark"
            mode="inline"
            selectedKeys={[this.state.currentRoute]}
            defaultOpenKeys={['sample']}
          >
            <Menu.Item key="sample">
              <Link to={`/${this.config.appName}/sample`}>
                <UserOutlined />
                <span>Sample</span>
              </Link>
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout>
          <Header className={styles.header}>
            <Menu
              mode="horizontal"
              className={styles.rightAlignedMenu}
              selectedKeys={[this.state.currentRoute]}
            >
              <Menu.SubMenu
                title={
                  <Avatar className={styles.avatar} icon={<UserOutlined />} />
                }
              >
                <Menu.ItemGroup title={this.config.user}>
                  <Menu.Item key="logout" onClick={this.logout}>
                    Logout
                  </Menu.Item>
                </Menu.ItemGroup>
              </Menu.SubMenu>
            </Menu>
          </Header>

          <RoutesContext.Provider
            value={{ setCurrentRoute: this.setCurrentRoute }}
          >
            <Content className={styles.content}>
              <Switch>
                {this.state.routes.map((route) => (
                  <RouteWithSubRoutes key={route.path} {...route} />
                ))}
              </Switch>
            </Content>
          </RoutesContext.Provider>
        </Layout>
      </Layout>
    );
  }
}

export default withConfigContext(Home);
