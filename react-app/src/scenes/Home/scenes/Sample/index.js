/*
 * Copyright (C) 2020. Entgra (Pvt) Ltd, https://entgra.io
 * All Rights Reserved.
 *
 * Unauthorized copying/redistribution of this file, via any medium
 * is strictly prohibited.
 * Proprietary and confidential.
 *
 * Licensed under the Entgra Commercial License,
 * Version 1.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 * You may obtain a copy of the License at
 * https://entgra.io/licenses/entgra-commercial/1.0
 */

import React from 'react';
import { Typography } from 'antd';
import styles from './styles.module.css';

const { Title, Paragraph, Text, Link } = Typography;

class Sample extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={styles.content}>
        <Typography>
          <Title>Getting Started Guide</Title>
          <Paragraph>
            Welcome to the getting started guide of Entgra base UI App.
          </Paragraph>
          <Title level={2}>How to rename application name</Title>
          <Paragraph>
            To change the Application name, you will have to modify the
            following places and re-build the application.
            <ul>
              <li>
                .war name defined in the pom.xml file. (In the base app it has
                defined as entgra-base-ui)
              </li>
              <li>
                appName config defined under public/config.json configuration
                file.
              </li>
            </ul>
          </Paragraph>
          <Paragraph>
            By referring to developed scenes, you can get a basic understanding
            of the component structure and{' '}
            <Text strong>
              you can refer to Antd components and use them to design your
              pages.
            </Text>
          </Paragraph>
          <Title level={2}>Guidelines and Resources</Title>
          <Paragraph>
            After building the WAR file deploy it in the{' '}
            <Text strong>IoTS webapps</Text> directory which is located under
            the <Text strong>[IoTS_HOME]repository/depoyment/server</Text>{' '}
            directory. To invoke APIs in the product, there should be an{' '}
            <Text strong>invoker web app</Text> in the IoTS webapps folder. In
            this base app, it has named the invoker web app as{' '}
            <Text strong>entgra-ui-request-handler</Text>. Let us say if you
            want to rename the invoker file, then you have to build the invoker
            component(i.e It is located in the carbon-device-mgt repository) and
            rename the war file as you like and deploy it in the IoTS webapps
            folder. Thereafter change the configurations defined in the{' '}
            <Text strong>public/config.json</Text>
            file as per the invoker rename and experience the UI
            functionalities.
          </Paragraph>

          <Paragraph>
            <ul>
              <li>
                <Link href="https://ant.design/components/overview/">
                  Antd Components
                </Link>
              </li>
              <li>
                <Link href="https://gitlab.com/entgra/base-ui">
                  Base UI App Repo
                </Link>
              </li>
              <li>
                <Link href="https://reactjs.org/docs/getting-started.html">
                  React JS
                </Link>
              </li>
            </ul>
          </Paragraph>
        </Typography>
      </div>
    );
  }
}

export default Sample;
