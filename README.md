# Getting Started Guide

Welcome to the getting started guide of Entgra base UI App.

## How to rename application name

To change the Application name, you will have to modify the following places and re-build the application.

1. .war name defined in the pom.xml file. (In the base app it has defined as entgra-base-ui)
2. appName config defined under public/config.json configuration
         
By referring to developed scenes, you can get a basic understanding of the component structure and 
**you can refer to Antd components and use them to design your pages.**

## Guidelines and Resources

After building the WAR file deploy it in the **IoTS webapps** directory which is located under
the **<IoTS_HOME>repository/depoyment/server** directory. 

To invoke APIs in the product, there should be an ** in the IoTS webapps folder. In this base app, it has named the 
invoker web app as **entgra-ui-request-handler**. Let us say if you want to rename the invoker file, then you 
have to build the invoker component(i.e It is located in the carbon-device-mgt repository) and rename 
the war file as you like and deploy it in the IoTS webapps folder. Thereafter, change the configurations defined 
in the **public/config.json** file as per the invoker rename and experience the UI functionalities.

1. [Antd Components](https://ant.design/components/overview/)
2. [React JS](https://reactjs.org/docs/getting-started.html)
